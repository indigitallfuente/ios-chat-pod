

Pod::Spec.new do |spec|

  spec.name         = 'indigitall-ios-chat'
  spec.version      = '1.6.4'
  spec.summary      = 'indigitall chat'


  spec.homepage     = 'https://bitbucket.org/indigitallfuente/ios-chat-pod'
  spec.license      = { :type => 'MIT', :file => 'LICENSE.md' }

  spec.author             = { 'Smart2me S.L.' => 'sdk@indigitall.com' }

  spec.source       = { :git => 'https://bitbucket.org/indigitallfuente/ios-chat-pod.git', :tag => spec.version.to_s }

  spec.ios.deployment_target = '12.0'
  spec.ios.vendored_frameworks = 'ios-chat.xcframework'
    spec.dependency 'Socket.IO-Client-Swift', '~> 16.1.0'

  spec.swift_version = '5.0'
end