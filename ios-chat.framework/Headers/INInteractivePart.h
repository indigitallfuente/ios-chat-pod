//
//  INInteractivePart.h
//  ios-chat
//
//  Created by indigitall on 18/1/22.
//  Copyright © 2022 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INInteractivePart : NSObject

@property (nonatomic, class, readonly) NSString *TYPE;
@property (nonatomic, class, readonly) NSString *TEXT;

@property (nonatomic) NSString *type;
@property (nonatomic) NSString *text;

- (id) initWithJson: (NSDictionary *)json;
+ (NSDictionary *)toJson:(INInteractivePart *)part;

@end

NS_ASSUME_NONNULL_END
