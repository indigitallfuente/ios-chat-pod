//
//  ViewController.h
//  ios-chat
//
//  Created by indigitall on 08/09/2020.
//  Copyright © 2020 indigitall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "INMessage.h"
#import "MessageReceivedProtocol.h"
#import "IndigitallChatView.h"
#import "TextFieldModel.h"
#import "ChatCellViewController.h"
#import "ChatProtocol.h"
#import "MessageReplyProtocol.h"

@interface ChatViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, NewMessageReceivedProtocol,
NewMessageReplyProtocol, UITextFieldDelegate, UIDocumentInteractionControllerDelegate>

@property (nonatomic)  NSMutableArray *listMessageArray;
@property (nonatomic, class, readonly) NSString *SHOW_INFO_SEGUE;
@property (nonatomic, class, readonly) NSString *SHOW_INTERACTIVE_LIST_SEGUE;

@property (weak, nonatomic) IBOutlet TextFieldModel *textField;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;

@property (nonatomic) MessageReplyProtocol *replyProtocol;

- (IBAction)backButton:(id)sender;

@property (nonatomic) SocketChat *socketChat;

@property (nonatomic) IndigitallChatView *parentView;

@property (nonatomic) ChatProtocol *chatProtocol;
@property (weak, nonatomic) IBOutlet UIButton *backButtonOutlet;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationBarTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (nonatomic) CGFloat originalConstant;

- (id) init;

- (void)newMessageReceived:(INMessage *)newMessage;
- (void)sendMessage: (INMessage *)message;
- (void)addMessage: (INMessage *) message;
- (void)setKeyboard;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;


@end


