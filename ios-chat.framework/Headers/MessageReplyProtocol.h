//
//  MessageReplyProtocol.h
//  ios-chat
//
//  Created by indigitall on 20/1/22.
//  Copyright © 2022 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INMessage.h"
NS_ASSUME_NONNULL_BEGIN

@protocol NewMessageReplyProtocol <NSObject>

- (void) newMessageReply: (INMessage *) message;

@end

@interface MessageReplyProtocol : NSObject{
    id delegate;
}

- (void) setDelegate:(id)newDelegate;
-(void) newMessageReply: (INMessage *) newMessage;

@end

NS_ASSUME_NONNULL_END
