//
//  INInteractive.h
//  ios-chat
//
//  Created by indigitall on 18/1/22.
//  Copyright © 2022 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInteractiveAction.h"
#import "INInteractivePart.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInteractive : NSObject

@property (nonatomic, class, readonly) NSString * TYPE;
@property (nonatomic, class, readonly) NSString * ACTION;

@property (nonatomic, class, readonly) NSString * HEADER;
@property (nonatomic, class, readonly) NSString * BODY;
@property (nonatomic, class, readonly) NSString * FOOTER;

@property (nonatomic, class, readonly) NSString * BUTTON;
@property (nonatomic, class, readonly) NSString * LIST;

@property (nonatomic) NSString* type;
@property (nonatomic) INInteractiveAction* action;
@property (nonatomic) INInteractivePart* header;
@property (nonatomic) INInteractivePart* body;
@property (nonatomic) INInteractivePart* footer;

- (id) initWithJson: (NSDictionary *)json;
+ (NSDictionary *)toJson:(INInteractive *)interactive;

@end

NS_ASSUME_NONNULL_END
