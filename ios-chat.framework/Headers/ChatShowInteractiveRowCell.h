//
//  ChatShowInteractiveRowCell.h
//  ios-chat
//
//  Created by indigitall on 20/1/22.
//  Copyright © 2022 indigitall. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatShowInteractiveRowCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *interactiveRowTitle;
@property (weak, nonatomic) IBOutlet UILabel *interactiveRowDescription;

@property (nonatomic) NSString *interactiveRowTitleString;
@property (nonatomic) NSString *interactiveRowDescriptionString;
@property (nonatomic) NSString *interactivePayload;

- (void) setup;

@end

NS_ASSUME_NONNULL_END
