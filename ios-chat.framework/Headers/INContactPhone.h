//
//  INContactPhone.h
//  ios-chat
//
//  Created by indigitall on 8/11/21.
//  Copyright © 2021 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INContactPhone : NSObject

@property (nonatomic, class, readonly) NSString *PHONE;
@property (nonatomic, class, readonly) NSString *TYPE;

@property (nonatomic) NSString* phone;
@property (nonatomic) NSString* type;

-(id) initWithJson:(NSDictionary *)json;
+ (NSDictionary *)toJson: (INContactPhone *)contactPhone;


@end

NS_ASSUME_NONNULL_END
