//
//  ChatCellViewController.h
//  chat-ios
//
//  Created by indigitall on 05/10/2020.
//  Copyright © 2020 indigitall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "INMessage.h"
#import "IndigitallChatView.h"
#import "ButtonModel.h"
#import "ios-chat/ios_chat-Swift.h"
#import "ChatViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatCellViewController : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *chatUserName;
@property (weak, nonatomic) IBOutlet UILabel *chatUserTime;
//@property (weak, nonatomic) IBOutlet UILabel *chatUserMessage;
@property (weak, nonatomic) IBOutlet UIView *chatUserViewMessage;
@property (weak, nonatomic) IBOutlet UITextView *chatUserTextViewMessage;

@property (weak, nonatomic) IBOutlet UIView *chatUserView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatUserViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *chatUserInteractiveView;
@property (weak, nonatomic) IBOutlet UILabel *chatUserInteractiveHeader;
@property (weak, nonatomic) IBOutlet UILabel *chatUserInteractiveBody;
@property (weak, nonatomic) IBOutlet UIView *chatUserInteractiveReplyView;
@property (weak, nonatomic) IBOutlet UILabel *chatUserInteractiveFooter;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatServerViewHeight;

@property (weak, nonatomic) IBOutlet UIView *chatServerViewMessage;
@property (weak, nonatomic) IBOutlet UILabel *chatServerName;
@property (weak, nonatomic) IBOutlet UILabel *chatServerTime;
@property (weak, nonatomic) IBOutlet UITextView *chatServerTextViewMessage;

@property (weak, nonatomic) IBOutlet UIView *chatServerView;
@property (weak, nonatomic) IBOutlet UIImageView *chatServerImage;

@property (weak, nonatomic) IBOutlet UIView *chatServerVideoView;
@property (weak, nonatomic) IBOutlet UIView *chatServerVideoContentView;
@property (weak, nonatomic) IBOutlet ButtonModel *chatServerVideoPlayPauseButton;
@property (weak, nonatomic) IBOutlet UIProgressView *chatServerVideoProgressView;


@property (weak, nonatomic) IBOutlet UIView *chatServerDocView;
@property (weak, nonatomic) IBOutlet UILabel *chatServerDocDownloadText;
@property (weak, nonatomic) IBOutlet UILabel *mymeType;
@property (weak, nonatomic) IBOutlet UIView *previewPDF;
@property (weak, nonatomic) IBOutlet UILabel *chatServerDocInfoLabel;

@property (weak, nonatomic) IBOutlet UIView *chatServerAudioView;
@property (weak, nonatomic) IBOutlet ButtonModel *chatServerAudioPlayButton;
@property (weak, nonatomic) IBOutlet UIProgressView *chatServerAudioProgressView;
@property (weak, nonatomic) IBOutlet UILabel *chatServerAudioTimeAudio;

@property (weak, nonatomic) IBOutlet UIView *chatButtonsView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatButtonsHeightConstraint;

@property (weak, nonatomic) IBOutlet UIImageView *chatServerLocationImageView;
@property (weak, nonatomic) IBOutlet UILabel *chatServerLocationHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *chatServerLocationSubLabel;
@property (weak, nonatomic) IBOutlet UIView *chatServerLocationView;

@property (weak, nonatomic) IBOutlet UIView *chatServerContactView;
@property (weak, nonatomic) IBOutlet UIImageView *chatServerContactIcon;
@property (weak, nonatomic) IBOutlet UILabel *chatServerContactName;
@property (weak, nonatomic) IBOutlet UIButton *chatServerContactAddContact;
- (IBAction)chatServerContactAddContactAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *chatServerContactNameIconView;
@property (nonatomic) NSString* infoContactIconColor;

@property (weak, nonatomic) IBOutlet UIView *chatServerInteractiveView;
@property (weak, nonatomic) IBOutlet UILabel *chatServerInteractiveHeader;
@property (weak, nonatomic) IBOutlet UILabel *chatServerInteractiveBody;
@property (weak, nonatomic) IBOutlet UILabel *chatServerInteractiveFooter;
@property (weak, nonatomic) IBOutlet UIView *chatServerInteractiveLineSeparator;
@property (weak, nonatomic) IBOutlet UIView *chatServerInteractiveButtonView;
@property (weak, nonatomic) IBOutlet UILabel *chatServerInteractiveButtonViewLabel;



@property (nonatomic) IndigitallChatView *indigitallChatView;
@property (weak, nonatomic) UIViewController *chatViewController;

- (void)setup:(INMessage *) message withAgent:(BOOL)agent socket:(SocketChat *)socketChat;
- (void)setSelected:(BOOL)selected;
- (void)awakeFromNib;
//- (CGFloat)getHeightServer;
//- (CGFloat)getHeightUser;
@end



NS_ASSUME_NONNULL_END
