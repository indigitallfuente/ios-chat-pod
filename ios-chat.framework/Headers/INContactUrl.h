//
//  INContactUrl.h
//  ios-chat
//
//  Created by indigitall on 8/11/21.
//  Copyright © 2021 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INContactUrl : NSObject

@property (nonatomic, class, readonly) NSString *URL;
@property (nonatomic, class, readonly) NSString *TYPE;

@property (nonatomic) NSString* url;
@property (nonatomic) NSString* type;

-(id) initWithJson:(NSDictionary *)json;
+ (NSDictionary *)toJson: (INContactUrl *)contactUrl;
@end

NS_ASSUME_NONNULL_END
