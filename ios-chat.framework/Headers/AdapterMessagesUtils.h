//
//  AdapterMessagesUtils.h
//  chat-ios
//
//  Created by indigitall on 09/10/2020.
//  Copyright © 2020 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INMessage.h"
#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import "ButtonModel.h"
#import "ChatViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface AdapterMessagesUtils : NSObject


+ (NSAttributedString *)setTextMessage: (NSString *)message;
+ (INMessage *)generateMessage: (NSString *)body;
+ (void)setImageMessage:(NSString *)url uIImageView:(UIImageView*)image;
+ (void)setVideoMessage:(NSString *)url caption:(NSString *)caption contentVideo:(UIView *)contentVideo playPause:(ButtonModel *)playPause progressBarView:(UIProgressView *)progressBarView;
+ (void)setDocumentMessages:(NSString *)docUrl contentId:(NSString *)contentId caption:(NSString *)caption parentView:(UIView *)parentView mimeType:(NSString *)mimeType infoLabel:(UILabel *)infoLabel previewPDF:(UIView *)previewPDF;
+ (NSString *) getMimeType: (NSString *)messageMimeType caption:(NSString *)caption mimeType:(UILabel *)mimeType downloadText:(UILabel *)downloadText;
+ (void)setAudioMessages:(NSString *)audioUrl caption:(NSString *)caption parentView:(UIView *)parentView button:(ButtonModel *)button progressBarView:(UIProgressView *)progressBarView timeAudio:(UILabel *)timeAudio;
//+ (void)setComponentsMessage:(INComponents *)components parentView:(UIView *)parentView;
+ (void)drawButton: (INComponents *)component parentView:(UIView *)parentView socketChat:(SocketChat *)socketChat;
+ (void) setLocationMessage:(UIView *)parentView location:(INLocationChat *)location header:(UILabel *)header subLabel:(UILabel *)subLabel icon: (UIImageView *)icon colorIcon:(NSString *)color;
+ (void) setContactMessage: (UIViewController*)chatViewController parentView: (UIView *)parentView contact:(INContact *)contact labelName:(UILabel *)labelName buttonAddContact:(UIButton *)buttonAddContact headerViewContact:(UIView *)headerViewContact addContactTextButton: (NSString *)addContactTextButton ;
+ (void) setInteractiveMessage:
(UIViewController*)chatViewController
            interactiveView:  (UIView *)interactiveView
            interactive:(INInteractive *)interactive
            header:(UILabel *)header
            body:(UILabel *)body
            footer:(UILabel *)footer
            buttonLabel:(nullable UILabel *)buttonLabel
            lineSeparator:(nullable UIView *) lineSeparator
            buttonView:(nullable UIView *) buttonView
            heightContraint:(nullable NSLayoutConstraint *) heightContraint
            chatButtonsView:(nullable UIView *)chatButtonsView
            chatButtonViewHeightContraint:(nullable NSLayoutConstraint *)chatButtonViewHeight
            socketChat:(SocketChat *)socketChat;
+ (NSString *)now;
@end

NS_ASSUME_NONNULL_END
