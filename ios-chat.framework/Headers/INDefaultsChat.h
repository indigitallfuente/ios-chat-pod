//
//  INDefaults.h
//  chat-ios
//
//  Created by indigitall on 16/09/2020.
//  Copyright © 2020 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INDefaultsChat : NSObject

@property (nonatomic)NSString *CHANNEL_KEY;
@property (nonatomic)NSString *SOCKET_ID_KEY;
@property (nonatomic)NSString *EXTERNAL_CODE_KEY;
@property (nonatomic)NSString *URL_SERVER_KEY;
@property (nonatomic)NSString *WELCOME_EMITTED;
@property (nonatomic)NSString *LOG_DEBUG;
@property (nonatomic)NSString *LOG_LEVEL;
@property (nonatomic)NSString *WELCOME_EMITTED_NAME;
@property (nonatomic)NSString *WELCOME_MESSAGE_SHOWED;

//- (id) init;
+ (void) setChannelKey:(NSString *) channelKey;
+ (NSString *) getChannelKey;

+ (void) setSocketId:(NSString *) socketId;
+ (NSString *) getSocketId;

+ (void) setExternalCode:(NSString *) externalCode;
+ (NSString *) getExternalCode;

+ (void) setURLServer:(NSString *) urlServer;
+ (NSString *) getURLServer;

+ (void) setWelcomeEmitted:(BOOL) wellcomeEmitted;
+ (BOOL) getWelcomeEmitted;

+ (void) setWelcomeEmittedName:(NSString *) wellcomeEmittedName;
+ (NSString *) getWelcomeEmittedName;

+ (void) setLogDebug:(BOOL) logDebug;
+ (BOOL) getLogDebug;

+ (void) setLogLevel:(int) logLevel;
+ (int) getLogLevel;

+ (void) setWelcomeMessageShowed:(BOOL) isWellcomeMessage;
+ (BOOL) isWelcomeMessageShowed;
@end

NS_ASSUME_NONNULL_END
