//
//  Content.h
//  chat-ios
//
//  Created by indigitall on 16/09/2020.
//  Copyright © 2020 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INComponents.h"
#import "INContact.h"
#import "INLocationChat.h"
#import "INInteractive.h"

NS_ASSUME_NONNULL_BEGIN

@interface INContent : NSObject
@property (nonatomic) NSString* contentId;
@property (nonatomic) NSString* url;
@property (nonatomic) NSString* mimeType;
@property (nonatomic) NSString* caption;
@property (nonatomic) NSString* body;
@property (nonatomic) NSString* previewUrl;
@property (nonatomic) NSArray<INComponents *>* components;
@property (nonatomic) INContact *contact;
@property (nonatomic) INLocationChat *location;
@property (nonatomic) INInteractive *interactive;

@property (nonatomic, class, readonly) NSString *CONTENT_ID;
@property (nonatomic, class, readonly) NSString *URL;
@property (nonatomic, class, readonly) NSString *MIMETYPE;
@property (nonatomic, class, readonly) NSString *CAPTION;
@property (nonatomic, class, readonly) NSString *BODY;
@property (nonatomic, class, readonly) NSString *PREVIEW_URL;
@property (nonatomic, class, readonly) NSString *COMPONENTS;
@property (nonatomic, class, readonly) NSString *CONTACTS;
@property (nonatomic, class, readonly) NSString *LOCATION;
@property (nonatomic, class, readonly) NSString *INTERACTIVE;

-(id) initWithJson:(NSDictionary *)json;
+ (NSDictionary *)toJson:(INContent *)content;

@end

NS_ASSUME_NONNULL_END
