//
//  INInteractiveSections.h
//  ios-chat
//
//  Created by indigitall on 18/1/22.
//  Copyright © 2022 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInteractiveSectionsRow.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInteractiveSections : NSObject

@property (nonatomic, class, readonly) NSString *ROWS;
@property (nonatomic, class, readonly) NSString *TITLE;

@property (nonatomic) NSString* title;
@property (nonatomic) NSArray<INInteractiveSectionsRow*> *rows;

- (id) initWithJson: (NSDictionary *)json;
+ (NSDictionary *)toJson:(INInteractiveSections *)sections;


@end

NS_ASSUME_NONNULL_END
