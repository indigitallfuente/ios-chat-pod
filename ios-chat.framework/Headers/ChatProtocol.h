//
//  ChatProtocol.h
//  chat-ios
//
//  Created by indigitall on 19/10/2020.
//  Copyright © 2020 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@protocol OnChatProtocol 
- (void) onChatShown;
- (void) onChatHidden;
@end

@interface ChatProtocol : NSObject{
    id delegate;
}
- (void) setDelegate:(id)newDelegate;
- (void) onChatShown;
- (void) onChatHidden;


@end

NS_ASSUME_NONNULL_END
