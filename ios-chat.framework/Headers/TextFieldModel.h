//
//  TextFieldModel.h
//  chat-ios
//
//  Created by indigitall on 13/10/2020.
//  Copyright © 2020 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TextFieldModel : UITextField

@end

NS_ASSUME_NONNULL_END
