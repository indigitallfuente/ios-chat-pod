// swift-tools-version:5.5
import PackageDescription
let package = Package(
    name: "ios-chat",
    products: [
        .library(
            name: "ios-chat",
            targets: ["ios-chat"])
    ],
    dependencies: [
        .package(url: "https://github.com/socketio/socket.io-client-swift", .upToNextMajor(from: "15.2.0"))
    ],
    targets: [
       .binaryTarget(
            name: "ios-chat",
            path: "ios-chat.xcframework.zip"
        )
    ]
)
