//
//  MessageReceivedProtocol.h
//  chat-ios
//
//  Created by indigitall on 16/09/2020.
//  Copyright © 2020 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INMessage.h"
NS_ASSUME_NONNULL_BEGIN

@protocol NewMessageReceivedProtocol <NSObject>

//- (Message *) newMessageReceived;
- (void) newMessageReceived: (INMessage *) newMessage;
@end

@interface MessageReceivedProtocol : NSObject{
    id delegate;
}
- (void) setDelegate:(id)newDelegate;
-(void) newMessageReceived: (INMessage *) newMessage;

@end

NS_ASSUME_NONNULL_END
