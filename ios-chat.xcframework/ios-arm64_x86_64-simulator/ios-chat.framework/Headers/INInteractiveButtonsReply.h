//
//  INInteractiveButtonsReply.h
//  ios-chat
//
//  Created by indigitall on 18/1/22.
//  Copyright © 2022 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INInteractiveButtonsReply : NSObject

@property (nonatomic, class, readonly) NSString *TITLE;
@property (nonatomic, class, readonly) NSString *PAYLOAD;

@property (nonatomic) NSString *title;
@property (nonatomic) NSString *payload;

- (id) initWithJson: (NSDictionary *)json;
+ (NSDictionary *)toJson:(INInteractiveButtonsReply *)buttonsReply;


@end

NS_ASSUME_NONNULL_END
