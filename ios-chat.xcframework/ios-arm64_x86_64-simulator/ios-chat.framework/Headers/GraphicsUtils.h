//
//  PropertiesUtils.h
//  chat-ios
//
//  Created by indigitall on 08/10/2020.
//  Copyright © 2020 indigitall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GraphicsUtils : NSObject
+ (UIColor *)colorFromHexString:(NSString *)hexString;
+ (BOOL) isColorDark:(UIColor *)color;
@end

NS_ASSUME_NONNULL_END
