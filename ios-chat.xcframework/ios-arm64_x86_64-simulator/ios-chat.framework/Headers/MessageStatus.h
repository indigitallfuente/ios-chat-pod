//
//  MessageStatus.h
//  chat-ios
//
//  Created by indigitall on 16/09/2020.
//  Copyright © 2020 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageStatus : NSObject

@property (nonatomic) NSString* idMessageStatus;
@property (nonatomic) NSString* status;
@property (nonatomic) NSString* statusAt;

@property (nonatomic, class, readonly) NSString * ID_MESSAGE_STATUS;
@property (nonatomic, class, readonly) NSString * STATUS;
@property (nonatomic, class, readonly) NSString * STATUS_AT;

@end

NS_ASSUME_NONNULL_END
