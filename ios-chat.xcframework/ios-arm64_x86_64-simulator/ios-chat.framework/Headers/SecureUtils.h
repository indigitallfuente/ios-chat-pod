//
//  INHmac.h
//  chat-ios
//
//  Created by indigitall on 16/09/2020.
//  Copyright © 2020 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCrypto.h>
#include <CommonCrypto/CommonDigest.h>
#include <CommonCrypto/CommonHMAC.h>

NS_ASSUME_NONNULL_BEGIN

@interface SecureUtils : NSObject
+ (NSString *)hmacsha256:(NSString *)data secret:(NSString *)key;
+ (NSString *)generateId;
@end

NS_ASSUME_NONNULL_END
