//
//  INContentReplyPayload.h
//  ios-chat
//
//  Created by indigitall on 26/2/21.
//  Copyright © 2021 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INContentReplyPayload : NSObject
@property (nonatomic, class, readonly) NSString * EVENT_NAME;
@property (nonatomic, class, readonly) NSString * QUICK_REPLY_EVENT;

@property (nonatomic) NSString *eventName;

- (id) init: (NSDictionary *)json;
- (id) initWithPayload: (NSString *)payload;
+ (NSDictionary *)toJson: (INContentReplyPayload *)contentPayload;

@end

NS_ASSUME_NONNULL_END
