//
//  INLocationChat.h
//  ios-chat
//
//  Created by indigitall on 8/11/21.
//  Copyright © 2021 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INLocationChat : NSObject

@property (nonatomic, class, readonly) NSString *NAME;
@property (nonatomic, class, readonly) NSString *ADDRESS;
@property (nonatomic, class, readonly) NSString *LATITUDE;
@property (nonatomic, class, readonly) NSString *LONGITUDE;

@property (nonatomic) NSString* name;
@property (nonatomic) NSString* address;
@property (nonatomic) NSString* latitude;
@property (nonatomic) NSString* longitude;

-(id) initWithJson:(NSDictionary *)json;
+ (NSDictionary *)toJson: (INLocationChat *)locationChat;

@end

NS_ASSUME_NONNULL_END
