//
//  Message.h
//  chat-ios
//
//  Created by indigitall on 16/09/2020.
//  Copyright © 2020 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INContent.h"

NS_ASSUME_NONNULL_BEGIN

@interface INMessage : NSObject

@property (nonatomic, class, readonly) NSString * MESSAGEID;
@property (nonatomic, class, readonly) NSString * TIMESPAN;
@property (nonatomic, class, readonly) NSString * SEND_TYPE;
@property (nonatomic, class, readonly) NSString * CONTENT_TYPE;
@property (nonatomic, class, readonly) NSString * CONTENT;
@property (nonatomic, class, readonly) NSString * ERROR;
@property (nonatomic, class, readonly) NSString * AGENT;

@property (nonatomic, class, readonly) NSString * NEW_MESSAGE_RECEIVED;

@property (nonatomic, class, readonly) NSString * TEXT;
@property (nonatomic, class, readonly) NSString * IMAGE;
@property (nonatomic, class, readonly) NSString * DOCUMENT;
@property (nonatomic, class, readonly) NSString * AUDIO;
@property (nonatomic, class, readonly) NSString * VIDEO;
@property (nonatomic, class, readonly) NSString * TEMPLATE;
@property (nonatomic, class, readonly) NSString * LOCATION;
@property (nonatomic, class, readonly) NSString * CONTACT;
@property (nonatomic, class, readonly) NSString * INTERACTIVE;

@property (nonatomic) NSString* messageId;
@property (nonatomic) NSString* timespan;
@property (nonatomic) NSString* sendType;
@property (nonatomic) NSString* contentType;
@property (nonatomic) INContent* content;
@property (nonatomic) NSString* error;

@property (nonatomic) BOOL agent;

- (id) initWithJson: (NSDictionary *)json;
+ (NSDictionary *)toJson:(INMessage *)message;
@end

NS_ASSUME_NONNULL_END
