//
//  INQuickReplyPayload.h
//  ios-chat
//
//  Created by indigitall on 17/2/21.
//  Copyright © 2021 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INContentReplyPayload.h"

NS_ASSUME_NONNULL_BEGIN

@interface INReplyPayload : NSObject
@property (nonatomic, class, readonly) NSString * CONVERSATION_SESSION_ID;
@property (nonatomic, class, readonly) NSString * CONTENT_TYPE;
@property (nonatomic, class, readonly) NSString * CONTENT;

@property (nonatomic) NSString *conversationSessionId;
@property (nonatomic) INContentReplyPayload *content;
@property (nonatomic) NSString *contentType;

- (id) init: (NSDictionary *)json;
- (id) initWithSocketId: (NSString *)socketId payload:(NSString *)payload event:(NSString *)event;

+ (NSDictionary *)toJson: (INReplyPayload *)payload;

@end

NS_ASSUME_NONNULL_END
