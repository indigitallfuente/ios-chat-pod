//
//  ChatShowInfoContactViewController.h
//  ios-chat
//
//  Created by indigitall on 9/11/21.
//  Copyright © 2021 indigitall. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "ChatShowInfoContactCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatShowInfoContactViewController :  UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *AddContacButtonOutlet;

- (IBAction)addContactButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *contactNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactBirthdayLabel;

@property (weak, nonatomic) IBOutlet UITableView *infoContactTableView;

@property (nonatomic) NSArray<ChatShowInfoContactCell *> * contactCells;

@property (nonatomic) INContact *contact;
@property (nonatomic) NSString *infoContactIconColor;
@property (nonatomic) NSString *infoContactTextButton;
@property (nonatomic) NSString *infoContactTopHeaderText;
@property (nonatomic) NSString *infoContactContactAddedText;

@property (weak, nonatomic) IBOutlet UIView *showInfoBackView;
@property (weak, nonatomic) IBOutlet UILabel *showInfoHeaderLabel;

@end

NS_ASSUME_NONNULL_END
