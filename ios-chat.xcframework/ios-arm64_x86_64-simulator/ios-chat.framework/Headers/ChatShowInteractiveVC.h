//
//  ChatShowInteractiveVC.h
//  ios-chat
//
//  Created by indigitall on 19/1/22.
//  Copyright © 2022 indigitall. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "INInteractive.h"
#import "ChatShowInteractiveCell.h"
#import "MessageReplyProtocol.h"
#import "ios-chat/ios_chat-Swift.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatShowInteractiveVC :  UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, class, readonly) NSString *CLICK_OPTION_SEGUE;

@property (weak, nonatomic) IBOutlet UIView *cancelButtonView;
@property (weak, nonatomic) IBOutlet UILabel *headerInteractiveList;
@property (weak, nonatomic) IBOutlet UITableView *interactiveListTableView;

@property (nonatomic) INInteractive *interactive;
@property (nonatomic) NSString *interactiveSectionsTitleColor;
@property (nonatomic) SocketChat *socketChat;
@property (nonatomic) MessageReplyProtocol* replyProtocol;

@property (nonatomic) NSArray<ChatShowInteractiveCell *> *interactiveCells;

- (void)cancelButtonPressed;

@end

NS_ASSUME_NONNULL_END
