//
//  ContactAddress.h
//  ios-chat
//
//  Created by indigitall on 8/11/21.
//  Copyright © 2021 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INContactAddress : NSObject

@property (nonatomic, class, readonly) NSString *ZIP;
@property (nonatomic, class, readonly) NSString *CITY;
@property (nonatomic, class, readonly) NSString *TYPE;
@property (nonatomic, class, readonly) NSString *STATE;
@property (nonatomic, class, readonly) NSString *STREET;
@property (nonatomic, class, readonly) NSString *COUNTRY;
@property (nonatomic, class, readonly) NSString *COUNTRY_CODE;

@property (nonatomic) NSString* zip;
@property (nonatomic) NSString* city;
@property (nonatomic) NSString* type;
@property (nonatomic) NSString* state;
@property (nonatomic) NSString* street;
@property (nonatomic) NSString* country;
@property (nonatomic) NSString* country_code;

-(id) initWithJson:(NSDictionary *)json;
+ (NSDictionary *)toJson: (INContactAddress *)contactAddress;

@end

NS_ASSUME_NONNULL_END
