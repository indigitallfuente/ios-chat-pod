//
//  ButtonModel.h
//  chat-ios
//
//  Created by indigitall on 10/10/2020.
//  Copyright © 2020 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import <WebKit/WebKit.h>
#import "INInteractiveButtons.h"
#import "INComponents.h"
#import "ios-chat/ios_chat-Swift.h"

NS_ASSUME_NONNULL_BEGIN

@interface ButtonModel : UIButton{
    WKWebView *webView;
    AVPlayer *player;
    ButtonModel *button;
    BOOL isPlaying;
    UILabel *time;
    INComponents *component;
    INInteractiveButtons *interactiveButtons;
    SocketChat *socketChat;
}
@property (nonatomic, readwrite, retain) WKWebView *webView;
@property (nonatomic, readwrite, retain) AVPlayer *player;
@property (nonatomic, readwrite, retain) ButtonModel *button;
@property (nonatomic, readwrite, retain) UILabel *time;
@property (nonatomic, readwrite, assign) BOOL isPlaying;
@property (nonatomic, readwrite, retain) INComponents *component;
@property (nonatomic, readwrite, retain) INInteractiveButtons *interactiveButtons;
@property (nonatomic, readwrite, retain) SocketChat *socketChat;

-(id)initWithFrame:(CGRect)frame;
@end

NS_ASSUME_NONNULL_END
