//
//  INInteractiveButtons.h
//  ios-chat
//
//  Created by indigitall on 18/1/22.
//  Copyright © 2022 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInteractiveButtonsReply.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInteractiveButtons : NSObject

@property (nonatomic, class, readonly) NSString *TYPE;
@property (nonatomic, class, readonly) NSString *REPLY;

@property (nonatomic) NSString* type;
@property (nonatomic) INInteractiveButtonsReply *reply;

- (id) initWithJson: (NSDictionary *)json;
+ (NSDictionary *)toJson:(INInteractiveButtons *)buttons;

@end

NS_ASSUME_NONNULL_END
