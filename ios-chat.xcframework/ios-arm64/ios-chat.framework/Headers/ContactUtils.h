//
//  ContactUtils.h
//  ios-chat
//
//  Created by indigitall on 9/11/21.
//  Copyright © 2021 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "INContact.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContactUtils : NSObject
+ (void) addContact: (INContact *)inContact contactAddedMessage:(NSString *)contactAddedMessage viewController:(UIViewController *)vc;
+ (void) contactAdded: (NSString *)contactAddedMessage viewController:(UIViewController *)vc;
@end

NS_ASSUME_NONNULL_END
