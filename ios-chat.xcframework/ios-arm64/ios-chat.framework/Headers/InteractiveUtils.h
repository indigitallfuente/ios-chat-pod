//
//  InteractiveUtils.h
//  ios-chat
//
//  Created by indigitall on 20/1/22.
//  Copyright © 2022 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInteractive.h"
#import "INMessage.h"
NS_ASSUME_NONNULL_BEGIN

@interface InteractiveUtils : NSObject
+ (INMessage *)createMessageWithInteractive:(INInteractive *)interactive withButton:(INInteractiveButtons *)button;
+ (INMessage *)createMessageWithInteractive:(INInteractive *)interactive withSectionRow: (INInteractiveSectionsRow *)row;
@end

NS_ASSUME_NONNULL_END
