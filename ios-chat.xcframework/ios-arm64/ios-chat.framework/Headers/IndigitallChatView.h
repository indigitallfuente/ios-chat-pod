//
//  IndigitallChatView.h
//  chat-ios
//
//  Created by indigitall on 06/10/2020.
//  Copyright © 2020 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ChatProtocol.h"

NS_ASSUME_NONNULL_BEGIN
IB_DESIGNABLE
@interface IndigitallChatView  : UIView

@property (weak, nonatomic) IBOutlet UIButton *chatButtonOutlet;
- (IBAction)chatButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIView *buttonContentView;

@property (nonatomic, nullable) IBInspectable NSString *channelKey;
@property (nonatomic, nullable) IBInspectable NSString *externalKey;
@property (nonatomic, nullable) IBInspectable NSString *urlServer;
@property (nonatomic, nullable) IBInspectable NSString *primaryColor;
@property (nonatomic, nullable) IBInspectable NSString *backgroundChatColor;
@property (nonatomic, nullable) IBInspectable NSString *messagePlaceholder;
@property (nonatomic, nullable) IBInspectable NSString *titleChat;
@property (nonatomic, nullable) IBInspectable NSString *botName;
@property (nonatomic, nullable) IBInspectable NSString *defaultUserName;
@property (nonatomic, nullable) IBInspectable NSString *openFileText;
@property (nonatomic) IBInspectable BOOL fullscreen;
@property (nonatomic, nullable) IBInspectable NSString *backgroundBarColor;
@property (nonatomic, nullable) IBInspectable UIImage *defaultChatIconResource;
@property (nonatomic, nullable) IBInspectable NSString *addContactTextButton;
@property (nonatomic, nullable) IBInspectable NSString *contactAddedMessage;
@property (nonatomic, nullable) IBInspectable NSString *infoContactIconsColor;
@property (nonatomic, nullable) IBInspectable NSString *infoContactTopBarTitle;
@property (nonatomic) IBInspectable int chatAutoOpenTime;
@property (nonatomic) IBInspectable BOOL clearMessages;
@property (nonatomic, nullable) IBInspectable NSString *welcomeEvent;
@property (nonatomic, nullable) IBInspectable NSString *welcomeMessage;
@property (nonatomic) IBInspectable BOOL cypher;
@property (nonatomic) IBInspectable BOOL disabledKeyboard;
@property (nonatomic) IBInspectable NSString * bottomConstraint;
@property (nonatomic) IBInspectable double x;
@property (nonatomic) IBInspectable double y;
@property (nonatomic) IBInspectable double width;
@property (nonatomic) IBInspectable double height;

@property (weak, nonatomic) IBOutlet UIView *chatViewContainer;

@property (nonatomic) ChatProtocol *chatProtocol;

@property (nonatomic)BOOL chatStarted;

- (void) clearAllMessages;
- (void) setCustomEvent: (NSString *)event;
- (BOOL) isWelcomeEmitted;

@end

NS_ASSUME_NONNULL_END
