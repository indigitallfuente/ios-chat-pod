//
//  LogLevel.h
//  ios-chat
//
//  Created by indigitall on 10/3/21.
//  Copyright © 2021 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum {
    debug,
    info,
    warning,
    error
} LogLevel;

extern NSString* _Nonnull const FormatType_toString[];

NS_ASSUME_NONNULL_END
