//
//  INContactOrg.h
//  ios-chat
//
//  Created by indigitall on 8/11/21.
//  Copyright © 2021 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INContactOrg : NSObject

@property (nonatomic, class, readonly) NSString *TITLE;
@property (nonatomic, class, readonly) NSString *COMPANY;
@property (nonatomic, class, readonly) NSString *DEPARMENT;

@property (nonatomic) NSString* title;
@property (nonatomic) NSString* company;
@property (nonatomic) NSString* department;

-(id) initWithJson:(NSDictionary *)json;
+ (NSDictionary *)toJson: (INContactOrg *)contactOrg;


@end

NS_ASSUME_NONNULL_END
