//
//  ChatShowInfoContactCell.h
//  ios-chat
//
//  Created by indigitall on 9/11/21.
//  Copyright © 2021 indigitall. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "INContact.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatShowInfoContactCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *infoContactIconCell;
@property (weak, nonatomic) IBOutlet UILabel *infoContactMainLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoContactSubLabel;

@property (nonatomic) NSString *infoContactIconName;
@property (nonatomic) NSString *infoContactMainText;
@property (nonatomic) NSString *infoContactSubText;

- (void) setupWithHeaderTextWithColor:(NSString *)color;

@end

NS_ASSUME_NONNULL_END
