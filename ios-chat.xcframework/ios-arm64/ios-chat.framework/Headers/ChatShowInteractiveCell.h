//
//  ChatShowInteractiveCell.h
//  ios-chat
//
//  Created by indigitall on 19/1/22.
//  Copyright © 2022 indigitall. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "INInteractive.h"
#import "ChatShowInteractiveRowCell.h"

#import "ios-chat/ios_chat-Swift.h"
#import "MessageReplyProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatShowInteractiveCell : UITableViewCell <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *interactiveCellTitle;
@property (weak, nonatomic) IBOutlet UITableView *interactiveRowTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *interactiveTableViewHeightContraint;

@property (nonatomic) NSString *interactiveCellTitleString;
@property (nonatomic) INInteractive *interactive;
@property (nonatomic) NSArray<INInteractiveSectionsRow *> *sectionRows;
@property (nonatomic) NSArray<ChatShowInteractiveRowCell *> *interactiveRowCells;
@property (nonatomic) MessageReplyProtocol* replyProtocol;
@property (nonatomic) NSString *interactiveSectionsTitleColor;

@property (nonatomic) SocketChat *socketChat;
@property (nonatomic) UIViewController *chatShowInteractiveVC;

- (void) setupInteractiveCellSection;

@end

NS_ASSUME_NONNULL_END
