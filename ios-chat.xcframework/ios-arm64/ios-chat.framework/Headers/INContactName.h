//
//  INContactName.h
//  ios-chat
//
//  Created by indigitall on 8/11/21.
//  Copyright © 2021 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INContactName : NSObject

@property (nonatomic, class, readonly) NSString *LAST_NAME;
@property (nonatomic, class, readonly) NSString *FIRST_NAME;
@property (nonatomic, class, readonly) NSString *FORMATTED_NAME;

@property (nonatomic) NSString* lastName;
@property (nonatomic) NSString* firstName;
@property (nonatomic) NSString* formattedName;

-(id) initWithJson:(NSDictionary *)json;
+ (NSDictionary *)toJson: (INContactName *)contactName;


@end

NS_ASSUME_NONNULL_END
