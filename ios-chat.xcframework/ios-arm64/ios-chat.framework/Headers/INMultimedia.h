//
//  INMultimedia.h
//  ios-chat
//
//  Created by indigitall on 17/2/21.
//  Copyright © 2021 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface INMultimedia : NSObject


@property (nonatomic, class, readonly) NSString * MULTIMEDIA_ID;
@property (nonatomic, class, readonly) NSString * URL;
@property (nonatomic, class, readonly) NSString * TYPE;
@property (nonatomic, class, readonly) NSString * MIME_TYPE;
@property (nonatomic, class, readonly) NSString * CAPTION;

@property (nonatomic, class, readonly) NSString * VIDEO;
@property (nonatomic, class, readonly) NSString * DOCUMENT;
@property (nonatomic, class, readonly) NSString * IMAGE;

@property (nonatomic) NSString *multimediaId;
@property (nonatomic) NSString *url;
@property (nonatomic) NSString* mimeType;
@property (nonatomic) NSString* type;
@property (nonatomic) NSString* caption;


typedef enum{
    video,
    document,
    image
}MultimediaType;

@property (nonatomic) MultimediaType MULTIMEDIA_TYPE;

- (id) initWithJson: (NSDictionary *)json;
+ (NSDictionary *)toJson:(INMultimedia *)multimedia;

@end

NS_ASSUME_NONNULL_END
