//
//  INContact.h
//  ios-chat
//
//  Created by indigitall on 8/11/21.
//  Copyright © 2021 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INContactOrg.h"
#import "INContactUrl.h"
#import "INContactName.h"
#import "INContactEmail.h"
#import "INContactPhone.h"
#import "INContactAddress.h"

NS_ASSUME_NONNULL_BEGIN

@interface INContact : NSObject

@property (nonatomic, class, readonly) NSString *ORG;
@property (nonatomic, class, readonly) NSString *NAME;
@property (nonatomic, class, readonly) NSString *BIRTHDAY;
@property (nonatomic, class, readonly) NSString *URLS;
@property (nonatomic, class, readonly) NSString *EMAILS;
@property (nonatomic, class, readonly) NSString *PHONES;
@property (nonatomic, class, readonly) NSString *ADDRESSES;

@property (nonatomic) NSString* birthday;
@property (nonatomic) INContactOrg* contactOrg;
@property (nonatomic) INContactName* contactName;
@property (nonatomic) NSArray<INContactEmail*> *contactEmailList;
@property (nonatomic) NSArray<INContactAddress*> *contactAddressList;
@property (nonatomic) NSArray<INContactPhone*> *contactPhoneList;
@property (nonatomic) NSArray<INContactUrl*> *contactUrlList;

-(id) initWithJson:(NSDictionary *)json;

@end

NS_ASSUME_NONNULL_END
