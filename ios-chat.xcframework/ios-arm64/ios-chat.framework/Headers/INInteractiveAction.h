//
//  INInteractiveAction.h
//  ios-chat
//
//  Created by indigitall on 18/1/22.
//  Copyright © 2022 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInteractiveSections.h"
#import "INInteractiveButtons.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInteractiveAction : NSObject

@property (nonatomic, class, readonly) NSString *SECTIONS;
@property (nonatomic, class, readonly) NSString *BUTTON;
@property (nonatomic, class, readonly) NSString *BUTTONS;

@property (nonatomic) NSString* button;
@property (nonatomic) NSArray<INInteractiveSections*> *sections;
@property (nonatomic) NSArray<INInteractiveButtons *> *buttons;

- (id) initWithJson: (NSDictionary *)json;
+ (NSDictionary *)toJson:(INInteractiveAction *)action;

@end

NS_ASSUME_NONNULL_END
