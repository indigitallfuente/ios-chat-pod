//
//  INComponents.h
//  ios-chat
//
//  Created by indigitall on 17/2/21.
//  Copyright © 2021 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INMultimedia.h"

NS_ASSUME_NONNULL_BEGIN

@interface INComponents : NSObject

@property (nonatomic, class, readonly) NSString *TYPE;
@property (nonatomic, class, readonly) NSString *TEXT;
@property (nonatomic, class, readonly) NSString *MULTIMEDIA;
@property (nonatomic, class, readonly) NSString *SUBTYPE;
@property (nonatomic, class, readonly) NSString *INDEX;
@property (nonatomic, class, readonly) NSString *LABEL;
@property (nonatomic, class, readonly) NSString *URL;
@property (nonatomic, class, readonly) NSString *PHONE;
@property (nonatomic, class, readonly) NSString *PAYLOAD;

@property (nonatomic, class, readonly) NSString * HEADER;
@property (nonatomic, class, readonly) NSString * BODY;
@property (nonatomic, class, readonly) NSString * FOOTER;
@property (nonatomic, class, readonly) NSString * BUTTON;

@property (nonatomic, class, readonly) NSString * PHONE_CALL;
@property (nonatomic, class, readonly) NSString * QUICK_REPLY;

@property (nonatomic) NSString* type;
@property (nonatomic) NSString* text;
@property (nonatomic) INMultimedia *multimedia;
@property (nonatomic) NSString* subType;
@property (nonatomic) NSString* index;
@property (nonatomic) NSString* label;
@property (nonatomic) NSString* url;
@property (nonatomic) NSString* phone;
@property (nonatomic) NSString* payload;

- (id) initWithJson: (NSDictionary *)json;
+ (NSDictionary *)toJson:(INComponents *)components;
@end

NS_ASSUME_NONNULL_END
