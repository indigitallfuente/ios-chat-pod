//
//  DownloadUtils.h
//  chat-ios
//
//  Created by indigitall on 09/10/2020.
//  Copyright © 2020 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DownloadUtils : NSObject
+ (void)downloadedImageFromURL: (NSURL *)url imageView: ( UIImageView*)imageView;
+ (void)downloadedMediaFromURL: (NSURL *)fileUrl mimeType:(NSString*)mimetype caption:caption url:(nullable void(^)(NSURL *url, long size))url ;
+(NSString *) randomStringWithLength: (int) len;
@end

NS_ASSUME_NONNULL_END
