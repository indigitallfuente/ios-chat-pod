//
//  LogUtils.h
//  ios-chat
//
//  Created by indigitall on 10/3/21.
//  Copyright © 2021 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LogLevel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LogUtils : NSObject

@property (nonatomic) int debug;
@property (nonatomic) int info;
@property (nonatomic) int warning;
@property (nonatomic) int error;

+ (void)setLogDebug:(BOOL)logDebug withLogLevel:(LogLevel )logLevel;

@end

NS_ASSUME_NONNULL_END
